/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/

#ifndef RUN4INTERFACES_IR4STEERINGTOOL_H
#define RUN4INTERFACES_IR4STEERINGTOOL_H 

#include "GaudiKernel/IAlgTool.h"

class IR4SteeringTool : virtual public IAlgTool {
 public:
  virtual StatusCode saySomething() = 0;

  DeclareInterfaceID(IR4SteeringTool, 1, 0);
};

#endif
