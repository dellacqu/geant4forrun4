/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/

#pragma once

#include "GaudiKernel/IAlgTool.h"

class IR4G4InitializationTool : virtual public IAlgTool {
 public:
  virtual StatusCode InitializeG4Sim() = 0;
  virtual StatusCode SimulateEvent() = 0;
  virtual StatusCode Terminate() =0;

  DeclareInterfaceID(IR4G4InitializationTool, 1, 0);
};

