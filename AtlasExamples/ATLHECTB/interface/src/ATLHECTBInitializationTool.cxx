/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/

#include "ATLHECTBInitializationTool.h"

// Detector construction, user actions

#include "ATLHECTBDetectorConstruction.hh"
#include "ATLHECTBActionInitialization.hh"

#include "G4RunManager.hh"
//    #include "G4RunManagerFactory.hh"
#include "G4UImanager.hh"
#include "FTFP_BERT.hh"

#include <mutex>
#include <thread>

ATLHECTBInitializationTool::ATLHECTBInitializationTool(const std::string &type, const std::string &name,
                     const IInterface *parent)
    : base_class(type, name, parent) {}

StatusCode ATLHECTBInitializationTool::InitializeG4Sim() 
{
   ATH_MSG_INFO("this is ATLHECTB's InitializeG4Sim ");

   ATH_MSG_INFO("Step 1: creating the G4RunManager");
     
// Set mandatory initialization classes
  auto runManager=G4RunManager::GetRunManager();

  m_detConstruction = std::make_shared<ATLHECTBDetectorConstruction> ();
  ATH_MSG_INFO(" detector construction "<<m_detConstruction);
  runManager->SetUserInitialization(m_detConstruction.get());

  m_physList = std::make_shared<FTFP_BERT> ();
  ATH_MSG_INFO(" physics list "<<m_physList);
  runManager->SetUserInitialization(m_physList.get());

// Set user action classes

  auto actionInitialization = new ATLHECTBActionInitialization();
  runManager->SetUserInitialization(actionInitialization);

   ATH_MSG_INFO("here follows a dump of the G4 macro that will be run ("<<m_g4Macro.size()<<") : ");
   if (m_g4Macro.size()) 
   {
     auto UImanager = G4UImanager::GetUIpointer();
     for (auto i : m_g4Macro)
     {
	ATH_MSG_INFO(" ---> "<<i);
        UImanager->ApplyCommand(i);
     }
   }
       

  return StatusCode::SUCCESS;
}

StatusCode ATLHECTBInitializationTool::SimulateEvent()
{
   ATH_MSG_INFO("this is ATLHECTB's SimulateEvent() ");

   return StatusCode::SUCCESS;
}

StatusCode ATLHECTBInitializationTool::Terminate()
{
   ATH_MSG_INFO("this is ATLHECTB's Terminate() ");

   return StatusCode::SUCCESS;
}
