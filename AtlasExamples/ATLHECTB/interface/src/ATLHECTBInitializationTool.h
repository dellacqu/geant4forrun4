/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/

#pragma once

#include <G4Run4Interfaces/IR4G4Initialization.h>
#include <AthenaBaseComps/AthAlgTool.h>

#include <string>

#include "G4VUserPhysicsList.hh"
#include "G4VUserDetectorConstruction.hh"
class G4RunManager;

class ATLHECTBInitializationTool : public extends<AthAlgTool, IR4G4InitializationTool > {
 public:
  ATLHECTBInitializationTool(const std::string &type, const std::string &name, const IInterface *parent);

  // the magic method this tool provides
  virtual StatusCode InitializeG4Sim() override;
  virtual StatusCode SimulateEvent() override;
  virtual StatusCode Terminate() override;

 private:

  G4RunManager* m_runManager=nullptr;

  std::shared_ptr<G4VUserDetectorConstruction> m_detConstruction;
  std::shared_ptr<G4VUserPhysicsList> m_physList;

  Gaudi::Property<std::vector<std::string>> m_g4Macro {
      this, "Geant4Macro", {} , "Geant4 macro commands"}; 
  int m_eventCounter=0;

};

