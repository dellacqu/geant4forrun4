# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration

from AthenaConfiguration.ComponentFactory import CompFactory
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator


def ATLHECTBInterfaceToolCfg(flags, name="ATLHECTBInitialization", **kwargs):
    result = ComponentAccumulator()
    tool=CompFactory.ATLHECTBInitializationTool(name,Geant4Macro=["/tracking/verbose 1",
                                                                  "/gun/particle mu+",
                                                                  "/gun/energy 10 GeV",
                                                                  "/gun/position 0 170 0 cm",
                                                                  "/gun/position 0 170 0 cm",
                                                                  "/run/initialize"]
                                          ,**kwargs)
    # create and add the tool to the result (and pass arguments)
    result.setPrivateTools(tool)
    return result


def Run4SteeringCfg(flags):
    result = ComponentAccumulator()

    alg = CompFactory.R4SteeringAlg("G4Run4Steering",

       G4InitializationTool=result.popToolsAndMerge(
                                   ATLHECTBInterfaceToolCfg(flags)),
    )
    # add the algorithm to the result
    result.addEventAlgo(alg)
    return result


if __name__ == "__main__":
    from AthenaConfiguration.MainServicesConfig import MainServicesCfg
    from AthenaConfiguration.AllConfigFlags import initConfigFlags
    flags = initConfigFlags()
    flags.Exec.MaxEvents = 2
    flags.fillFromArgs()
    flags.lock()

    cfg = MainServicesCfg(flags)
    cfg.merge(Run4SteeringCfg(flags))
    cfg.run()
