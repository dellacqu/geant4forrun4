###############################################################
#
# Job options file
#
#==============================================================

#--------------------------------------------------------------
# ATLAS default Application Configuration options
#--------------------------------------------------------------
import AthenaCommon.AtlasUnixStandardJob

#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------

# Full job is a list of algorithms
from AthenaCommon.AlgSequence import AlgSequence
job = AlgSequence()

# Add top algorithms to be run
from G4Run4Steering.G4Run4SteeringConf import R4SteeringAlg
job += R4SteeringAlg( "G4R4Steering" )   # 1 alg, named "HelloWorld"

#--------------------------------------------------------------
# Set output level threshold (DEBUG, INFO, WARNING, ERROR, FATAL)
#--------------------------------------------------------------

# Output level for HelloAlg only (note name: instance, not type)
G4R4Steering = job.G4R4Steering
G4R4Steering.OutputLevel = INFO
G4R4Steering.SimSteering = False

# You can set the global output level on the message svc (not
# recommended) or by using the -l athena CLI parameter

#--------------------------------------------------------------
# Event related parameters
#--------------------------------------------------------------

# Number of events to be processed (default is until the end of
# input, or -1, however, since we have no input, a limit needs
# to be set explicitly, here, choose 10)
theApp.EvtMax = 10

#--------------------------------------------------------------
# Algorithms Private Options (all optional)
#--------------------------------------------------------------


#--------------------------------------------------------------
# Algorithms Tool Usage Private Options (advanced and optional)
#--------------------------------------------------------------

# Import configurable for using our HelloTool
from G4Run4Steering.G4Run4SteeringConf import R4G4InitializationTool

# Setup a public tool so that it can be used (again, note name)
ToolSvc += R4G4InitializationTool( "DefaultInitTool" )
ToolSvc.DefaultInitTool.MyMessage = "Madonna puttana"

# 
# G4R4Steering.G4InitializationTool = ( "R4G4InitializationTool" )
G4R4Steering.G4InitializationTool = ToolSvc.DefaultInitTool
# G4R4Steering.G4InitializationTool.MyMessage = "vaffancuuuuuuuulooooooooo"

#==============================================================
#
# End of job options file
#
###############################################################

