# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration

from AthenaConfiguration.ComponentFactory import CompFactory
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator


def Run4SteeringToolCfg(flags, name="B4aInitializationTool", **kwargs):
    result = ComponentAccumulator()
    # create and add the tool to the result (and pass arguments)
    result.setPrivateTools(CompFactory.B4aInitializationTool(name, **kwargs), primary=True)
    return result


def Run4SteeringCfg(flags):
    result = ComponentAccumulator()

    alg = CompFactory.R4SteeringAlg("G4Run4Steering",
                               G4InitializationTool="B4aInitializationTool/B4aInitializationTool",
                               )

    # add the algorithm to the result
    result.addEventAlgo(alg)
    return result


if __name__ == "__main__":
    from AthenaConfiguration.MainServicesConfig import MainServicesCfg
    from AthenaConfiguration.AllConfigFlags import initConfigFlags
    flags = initConfigFlags()
    flags.Exec.MaxEvents = 10
    flags.fillFromArgs()
    flags.lock()

    cfg = MainServicesCfg(flags)
    cfg.merge(Run4SteeringCfg(flags))
    cfg.run()
