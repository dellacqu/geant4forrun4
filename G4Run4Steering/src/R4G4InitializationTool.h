/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/

#pragma once

#include <G4Run4Interfaces/IR4G4Initialization.h>
#include <AthenaBaseComps/AthAlgTool.h>

#include <string>

class R4G4InitializationTool : public extends<AthAlgTool, IR4G4InitializationTool > {
 public:
  R4G4InitializationTool(const std::string &type, const std::string &name, const IInterface *parent);

  // the magic method this tool provides
  virtual StatusCode InitializeG4Sim() override;
  virtual StatusCode SimulateEvent() override;
  virtual StatusCode Terminate() override;

 private:
  Gaudi::Property<std::string> m_myMessage{this, "MyMessage","Porco dio!!!!!!!",
                                           "Default message set in R4SteeringTool.h"};
};

