/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/

#include "../R4SteeringAlg.h"
#include "../R4G4InitializationTool.h"

DECLARE_COMPONENT(R4SteeringAlg)
DECLARE_COMPONENT(R4G4InitializationTool)
