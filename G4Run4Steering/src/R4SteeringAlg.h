/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/

#pragma once

#include "AthenaBaseComps/AthAlgorithm.h"
#include "Gaudi/Property.h"
#include "GaudiKernel/ToolHandle.h"

#include <G4Run4Interfaces/IR4G4Initialization.h>

#include <map>
#include <string>
#include <utility>
#include <vector>

#include "G4RunManager.hh"

class R4SteeringAlg : public AthAlgorithm {
 public:
  R4SteeringAlg(const std::string &name, ISvcLocator *pSvcLocator);

  virtual ~R4SteeringAlg(); 
  virtual StatusCode initialize() override;
  virtual StatusCode execute() override;
  virtual StatusCode finalize() override;
  virtual StatusCode SimulateEvent();
  virtual StatusCode Terminate();

  ToolHandle<IR4G4InitializationTool> m_myPrivateR4SteeringTool;
//        {this,"MyPrivateR4SteeringTool","R4SteeringTool","private IR4G4InitializationTool"};

 private:

  int m_eventCounter=0;

  G4RunManager* m_runManager=nullptr;

  // Properties
  Gaudi::Property<bool> m_simSteering{this, "SimSteering", true, 
      "Flag that allows switching between tool and alg for simulation steering"};

  Gaudi::Property<std::vector<std::string>> m_g4UICommands{
      this, "G4_UI_Commands", {}, "A series of macro commands to steer G4"};

};

