/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/

#include "R4SteeringAlg.h"

#include <G4Run4Interfaces/IR4G4Initialization.h>

#include <iterator>

#include "G4RunManager.hh"
#include "G4GeometryManager.hh"




R4SteeringAlg::R4SteeringAlg(const std::string &name, ISvcLocator *pSvcLocator)
    : AthAlgorithm(name, pSvcLocator),m_myPrivateR4SteeringTool("R4G4InitializationTool/GenericR4SteeringTool", this) 
{
//  ATH_MSG_INFO("!!!! this is R4SteeringAlg constructor !!!!");
  //m_runManager=new G4RunManager;
  declareProperty("G4InitializationTool",m_myPrivateR4SteeringTool,"Tool to initialize G4");
}

R4SteeringAlg::~R4SteeringAlg()
{  
}

StatusCode R4SteeringAlg::initialize() {
  // Part 1: print where you are
  ATH_MSG_INFO("initialize()");
  
  m_runManager=new G4RunManager;
  ATH_CHECK(m_myPrivateR4SteeringTool.retrieve());
  ATH_CHECK(m_myPrivateR4SteeringTool->InitializeG4Sim());
  ATH_MSG_INFO ( " exiting initialization method " );

  return StatusCode::SUCCESS;
}

StatusCode R4SteeringAlg::execute() {
  ATH_MSG_INFO("execute()");
  if (m_simSteering)
    ATH_CHECK(SimulateEvent());
  else
    ATH_CHECK(m_myPrivateR4SteeringTool->SimulateEvent());

  ATH_MSG_INFO ( " exiting execute method " );
  return StatusCode::SUCCESS;
}

StatusCode R4SteeringAlg::finalize() {
  ATH_MSG_INFO("finalize()");
  
  if (m_simSteering)
    ATH_CHECK(Terminate());
  else
    ATH_CHECK(m_myPrivateR4SteeringTool->Terminate());

  return StatusCode::SUCCESS;
}

StatusCode R4SteeringAlg::SimulateEvent() {

   ATH_MSG_INFO("this is SimulateEvent() ");

   std::once_flag iEntry;

   std::call_once(iEntry, []()
   {
        auto* rM = G4RunManager::GetRunManager();
        G4bool cond = rM->ConfirmBeamOnCondition();
        if (cond)
        {
          rM->ConstructScoringWorlds();
          rM->RunInitialization();
        }
   });
   m_eventCounter++;
   m_runManager->ProcessOneEvent(m_eventCounter);
   m_runManager->TerminateOneEvent();

   return StatusCode::SUCCESS;
}

StatusCode R4SteeringAlg::Terminate() {

   m_runManager->TerminateEventLoop();
   m_runManager->RunTermination();
   G4GeometryManager::GetInstance()->OpenGeometry();
   // delete m_runManager;
   ATH_MSG_INFO("Done!!");
   return StatusCode::SUCCESS;
}
