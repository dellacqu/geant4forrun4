/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/

#include "R4G4InitializationTool.h"

R4G4InitializationTool::R4G4InitializationTool(const std::string &type, const std::string &name,
                     const IInterface *parent)
    : base_class(type, name, parent) {}

StatusCode R4G4InitializationTool::InitializeG4Sim() 
{
   ATH_MSG_INFO("this is InitializeG4Sim ");
  ATH_MSG_INFO("my message to the world: " << m_myMessage.value());

  return StatusCode::SUCCESS;
}

StatusCode R4G4InitializationTool::SimulateEvent()
{
    ATH_MSG_INFO(" this is R4G4InitializationTool's SimulateEvent() ");
    return StatusCode::SUCCESS;
}

StatusCode R4G4InitializationTool::Terminate()
{
    ATH_MSG_INFO(" this is R4G4InitializationTool's Terminate() ");
    return StatusCode::SUCCESS;
}
