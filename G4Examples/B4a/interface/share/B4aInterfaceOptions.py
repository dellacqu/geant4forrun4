###############################################################
#
# Job options file
#
#==============================================================

#--------------------------------------------------------------
# ATLAS default Application Configuration options
#--------------------------------------------------------------
import AthenaCommon.AtlasUnixStandardJob

#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------

# Full job is a list of algorithms
from AthenaCommon.AlgSequence import AlgSequence
job = AlgSequence()

# Add top algorithms to be run
from G4Run4Steering.G4Run4SteeringConf import R4SteeringAlg
job += R4SteeringAlg "B4a_example" )   # 1 alg, named "HelloWorld"

#--------------------------------------------------------------
# Set output level threshold (DEBUG, INFO, WARNING, ERROR, FATAL)
#--------------------------------------------------------------

# Output level for HelloAlg only (note name: instance, not type)
job.B4a_example.OutputLevel = INFO

# You can set the global output level on the message svc (not
# recommended) or by using the -l athena CLI parameter

#--------------------------------------------------------------
# Event related parameters
#--------------------------------------------------------------

# Number of events to be processed (default is until the end of
# input, or -1, however, since we have no input, a limit needs
# to be set explicitly, here, choose 10)
theApp.EvtMax = 10

#--------------------------------------------------------------
# Algorithms Private Options (all optional)
#--------------------------------------------------------------

# For convenience, get a reference to the HelloAlg Algorithm
# named "HelloWorld" in the job
B4a_example = job.B4a_example

#--------------------------------------------------------------
# Algorithms Tool Usage Private Options (advanced and optional)
#--------------------------------------------------------------

# Import configurable for using our HelloTool
from B4a.B4aInterfaceConf import B4aInitializationTool

# Setup a public tool so that it can be used (again, note name)
ToolSvc += B4aInitializationTool( "B4aInitialization" )


# Tell "HelloWorld" to use this tool ("MyPublicHelloTool" is a
# ToolHandle property of HelloAlg)
B4a_example.G4InitializationTool = ToolSvc.B4aInitializationTool

#==============================================================
#
# End of job options file
#
###############################################################

