###############################################################
#
# Job options file
#
#==============================================================

#--------------------------------------------------------------
# ATLAS default Application Configuration options
#--------------------------------------------------------------
import AthenaCommon.AtlasUnixStandardJob

#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------

# Full job is a list of algorithms
from AthenaCommon.AlgSequence import AlgSequence
job = AlgSequence()

# Add top algorithms to be run
from G4Run4Steering.G4Run4SteeringConf import R4SteeringAlg
job += R4SteeringAlg( "ATLHECTB_sim" )   # 1 alg, named "HelloWorld"

#--------------------------------------------------------------
# Set output level threshold (DEBUG, INFO, WARNING, ERROR, FATAL)
#--------------------------------------------------------------

# Output level for HelloAlg only (note name: instance, not type)
job.ATLHECTB_sim.OutputLevel = INFO

# You can set the global output level on the message svc (not
# recommended) or by using the -l athena CLI parameter

#--------------------------------------------------------------
# Event related parameters
#--------------------------------------------------------------

# Number of events to be processed (default is until the end of
# input, or -1, however, since we have no input, a limit needs
# to be set explicitly, here, choose 10)
theApp.EvtMax = 10

#--------------------------------------------------------------
# Algorithms Private Options (all optional)
#--------------------------------------------------------------

# For convenience, get a reference to the HelloAlg Algorithm
# named "HelloWorld" in the job
ATLHECTB_sim = job.ATLHECTB_sim


#--------------------------------------------------------------
# Algorithms Tool Usage Private Options (advanced and optional)
#--------------------------------------------------------------

# Import configurable for using our HelloTool

from ATLHECTB.ATLHECTBInterfaceConf import ATLHECTBInitializationTool

# Setup a public tool so that it can be used (again, note name)
ToolSvc += ATLHECTBInitializationTool( "ATLHECTBInitialization" )
ToolSvc.ATLHECTBInitialization.Geant4Macro=["/tracking/verbose 1",
                                            "/gun/particle mu+",
                                            "/gun/energy 10 GeV",
                                            "/gun/position 0 170 0 cm",
                                            "/gun/position 0 170 0 cm",
                                            "/run/initialize"]
#  ToolSvc.ATLHECTBInitialization.MyMessage = "A Public Message!"

# set ATLHECTB_sim to use tool ATLHECTBInitialization
# 
ATLHECTB_sim.G4InitializationTool = ToolSvc.ATLHECTBInitialization

#==============================================================
#
# End of job options file
#
###############################################################

