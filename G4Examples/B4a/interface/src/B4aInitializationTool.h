/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/

#pragma once

#include <G4Run4Interfaces/IR4G4Initialization.h>
#include <AthenaBaseComps/AthAlgTool.h>

#include <string>

class B4aInitializationTool : public extends<AthAlgTool, IR4G4InitializationTool > {
 public:
  B4aInitializationTool(const std::string &type, const std::string &name, const IInterface *parent);

  // the magic method this tool provides
  virtual StatusCode InitializeG4Sim() override;
  virtual StatusCode SimulateEvent() override;
  virtual StatusCode Terminate() override;

 private:

  Gaudi::Property<std::vector<std::string>> m_g4Macro {
      this, "Geant4Macro", {} , "Geant4 macro commands"}; 
};

