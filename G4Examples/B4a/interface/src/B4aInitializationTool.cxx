/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/

#include "B4aInitializationTool.h"

// Detector construction, user actions

#include "DetectorConstruction.hh"
#include "ActionInitialization.hh"

#include "G4RunManager.hh"
//    #include "G4RunManagerFactory.hh"
#include "G4UImanager.hh"
#include "FTFP_BERT.hh"

B4aInitializationTool::B4aInitializationTool(const std::string &type, const std::string &name,
                     const IInterface *parent)
    : base_class(type, name, parent) {}

StatusCode B4aInitializationTool::InitializeG4Sim() 
{
   ATH_MSG_INFO("this is B4a's InitializeG4Sim ");

   ATH_MSG_INFO("Step 1: creating the G4RunManager");
     
// Construct the default run manager
//
   auto* runManager = G4RunManager::GetRunManager();

// Set mandatory initialization classes

  auto detConstruction = new B4::DetectorConstruction();
  runManager->SetUserInitialization(detConstruction);

  auto physicsList = new FTFP_BERT;
  runManager->SetUserInitialization(physicsList);

// Set user action classes

   auto actionInitialization = new B4a::ActionInitialization(detConstruction);
   runManager->SetUserInitialization(actionInitialization);

   ATH_MSG_INFO("here follows a dump of the G4 macro that will be run ("<<m_g4Macro.size()<<") : ");
   if (m_g4Macro.size()) 
   {
     auto UImanager = G4UImanager::GetUIpointer();
     for (auto i : m_g4Macro)
     {
	ATH_MSG_INFO(" ---> "<<i);
        UImanager->ApplyCommand(i);
     }
   }
       

  return StatusCode::SUCCESS;
}

StatusCode B4aInitializationTool::SimulateEvent()
{
   return StatusCode::SUCCESS;
}
StatusCode B4aInitializationTool::Terminate()
{
   return StatusCode::SUCCESS;
}
