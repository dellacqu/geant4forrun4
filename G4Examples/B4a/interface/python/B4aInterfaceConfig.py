# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration

from AthenaConfiguration.ComponentFactory import CompFactory
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator


def B4aInterfaceToolCfg(flags, name="B4aInitialization", **kwargs):
    result = ComponentAccumulator()
    tool=CompFactory.B4aInitializationTool(name,Geant4Macro=["/tracking/verbose 1",
                                                             "/gun/particle mu+",
                                                             "/gun/energy 300 MeV",
                                                             "/process/em/verbose 0",
                                                             "/process/had/verbose 0",
                                                             "/run/initialize"]
                                          ,**kwargs)
    # create and add the tool to the result (and pass arguments)
    result.setPrivateTools(tool)
    return result


def Run4SteeringCfg(flags):
    result = ComponentAccumulator()

    alg = CompFactory.R4SteeringAlg("G4Run4Steering",

       G4InitializationTool=result.popToolsAndMerge(
                                   B4aInterfaceToolCfg(flags)),
    )
    # add the algorithm to the result
    result.addEventAlgo(alg)
    return result


if __name__ == "__main__":
    from AthenaConfiguration.MainServicesConfig import MainServicesCfg
    from AthenaConfiguration.AllConfigFlags import initConfigFlags
    flags = initConfigFlags()
    flags.Exec.MaxEvents = 2
    flags.fillFromArgs()
    flags.lock()

    cfg = MainServicesCfg(flags)
    cfg.merge(Run4SteeringCfg(flags))
    cfg.run()
