# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration

# Declare the package name:
atlas_subdir( B4a )

# External dependencies:
find_package( CLHEP REQUIRED )
find_package( Geant4 )
message( STATUS "Found CLHEP: ${CLHEP_LIBRARIES}  ${CLHEP_INCLUDE_DIRS}")

# Component(s) in the package:
atlas_add_component( B4aInterface
                     include_B4a/*.hh src/*.cc interface/src/*.cxx interface/src/components/*.cxx
                     INCLUDE_DIRS include_B4a ${GEANT4_INCLUDE_DIRS} 
                     LINK_LIBRARIES ${CLHEP_LIBRARIES} ${GEANT4_LIBRARIES} AthenaBaseComps AthenaKernel G4Run4Interfaces)

atlas_add_library( B4a
                     include_B4a/*.hh src/*.cc
                     OBJECT
                     PUBLIC_HEADERS include_B4a
                     INCLUDE_DIRS include_B4a ${GEANT4_INCLUDE_DIRS} 
                     LINK_LIBRARIES ${GEANT4_LIBRARIES} ${CLHEP_LIBRARIES} )

atlas_add_executable( B4a.exe *.cc src/*.cc
                     INCLUDE_DIRS include_B4a ${GEANT4_INCLUDE_DIRS} 
                     LINK_LIBRARIES ${GEANT4_LIBRARIES} ${CLHEP_LIBRARIES} )

# Install files from the package:
atlas_install_joboptions( interface/share/*.py )
atlas_install_python_modules( interface/python/B4aInterfaceConfig.py 
                              POST_BUILD_CMD ${ATLAS_FLAKE8} )

# Test(s) in the package:
